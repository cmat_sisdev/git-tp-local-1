# Git TP utilisation local - faire un dépôt 

## Un point de configuration 

Se présenter à GIT 

```bash 
$ git config --global init.defaultbranch main
$ git config --global user.name 'Nom Prenom'
$ git config --global user.email prenom.nom@fake-server.fr
```

Si vous n'aimez pas Vi

```bash
$ git config --global core.editor nano
```

## Objectif du TP 

1. Créer un dépôt 
2. Y ajouter des fichiers (2 ou 3 avec le contenu que vous souhaitez) et faire un premier commit 
3. Configurer GIT pour ignorer les fichiers ayant l'extension `.o`
4. Vérifier que l'étape 3 fonctionne 
5. Renommer un fichier créé à l'étape 2 et faire un nouveau commit 


## Aide : les commandes nécessaires 

```
git init 
git add <filename>
git mv 
git status 
```

